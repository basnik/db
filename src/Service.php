<?php

namespace Basnik\Db;

/**
 * Base service class to be inherited by database services, that fetch data from database and write the data to database.
 * Provides some sugar methods, that cover the most commonly used read/write cases.
 */
abstract class Service {

	/** Provides snake case convert method that can be handy for service implementations */
	use TSnakeCase;

	/** @var bool Used for sorting, represents ascending sorting order */
	const ASC = true;
	/** @var bool Used for sorting, represents descending sorting order */
	const DESC = false;

	/** @var \Dibi\Connection Database connection to work with */
	protected $db;

	/** @var string Name of main database table to be used by sugar methods in this class */
	protected $mainTable;

	/** @var string Class of database entity, representing table rows. */
	protected $entityClass;

	/**
     * Constructor forces the table name and object class to be filled.
	 * 
	 * @param \Dibi\Connection $db Dibi database connection
	 * @param string $mainTable Name of main database table to be used by sugar methods in this class
	 * @param string $entityClass Class of database entity, representing table rows. Must inherit Entity class. Use `YourClass::class`.
     */
	public function __construct(\Dibi\Connection $db, string $mainTable, string $entityClass) {
		$this->db = $db;
		$this->mainTable = $mainTable;

		if (!is_subclass_of($entityClass, Entity::class)) {
			throw new InvalidClassException("Unable to create database service, object class $entityClass does not inherit Entity base class.");
		}
		
		$this->entityClass = $entityClass;
	}

	/**
	 * Saves given object into the database table given by `mainTable` property.
	 * If `id` property is not null, it will do update instead.
	 * If `id` property is null, it will be set to id of newly created database row.
	 * Returns given entity (fluent).
	 */
	public function save(Entity $objectToSave) {
		return $this->saveObjectTo($objectToSave);
	}

	/**
	 * Saves multiple objects to the database table given by `mainTable` property, using INSERT INTO VALUES (),(),() syntax.
	 * Cannot be used to update the objects, `id` property won't be updated.
	 * It is not fluent, returns void.
	 */
	public function insertMultiple(array $objectsToSave) {
		$arrays = array_map(function($object){ return $object->toArray(); }, $objectsToSave);
		$this->db->query("INSERT INTO %n %m", $this->mainTable, $arrays);
	}

	/**
	 * Deletes a row with given id in database table given by `mainTable` property.
	 * It is not fluent, returns void.
	 */
	public function deleteById($id) {
		$this->db->query("DELETE FROM %n WHERE id = %i LIMIT 1", $this->mainTable, $id);
	}

	/**
	 * Returns object of class defined in `entityClass` property.
	 * The object represents database row and it is taken from table given by `mainTable` property.
	 * Row with same id as given `$id` parameter will be returned.
	 */
	public function getById($id) {
		return $this->rowToObject($this->db->fetch("SELECT * FROM %n WHERE id = %i", $this->mainTable, $id));
	}

	/**
	 * Returns objects of class defined in `entityClass` property.
	 * The objects represent database rows and are taken from table given by `mainTable` property.
	 * All rows with ids in given `$ids` array parameters will be returned.
	 */
	public function getAllByIds(array $ids) {
		return $this->rowsToObjects($this->db->fetchAll("SELECT * FROM %n WHERE id IN %l", $this->mainTable, $ids));
	}

	/**
	 * Returns objects of class defined in `entityClass` property.
	 * The objects represent database rows and are taken from table given by `mainTable` property.
	 * All objects in given table will be returned, sorted by given order. Default order is by id, ascending.
	 */
	public function getAll(array $order = ["id" => self::ASC], $limit = null, $offset = null) {
		return $this->rowsToObjects($this->db->fetchAll("SELECT * FROM %n ORDER BY %by %lmt %ofs",
			$this->mainTable, $order, $limit, $offset));
	}

	/**
	 * Returns objects of class defined in `entityClass` property.
	 * The objects represent database rows and are taken from table given by `mainTable` property.
	 * All objects that match criteria given by $andWhere parameter will be returned, sorted by given order. Default order is by id, ascending.
	 */
	public function getAllMatching(array $andWhere, array $order = ["id" => self::ASC], $limit = null, $offset = null) {
		return $this->rowsToObjects($this->db->fetchAll("SELECT * FROM %n WHERE %and ORDER BY %by %lmt %ofs",
			$this->mainTable, $andWhere, $order, $limit, $offset));
	}

	/**
	 * Counts all rows in table given by `mainTable` property that match criteria given by $andWhere parameter.
	 * Number is returned.
	 */
	public function countAllMatching(array $andWhere) {
		return $this->db->fetchSingle("SELECT COUNT(*) FROM %n WHERE %and", $this->mainTable, $andWhere);
	}

	/**
	 * Helper that converts database query result to target entity class object.
	 * By default it converts the result to objects of `$entityClass` class, but you can specify you own target class
	 * that must be subclass of Entity class. Use `YourClass::class`.
	 * Can be also used for example when handling related entities in your service.
	 */
	protected function rowToObject($rowResult, string $targetEntityClass = null) {
		// dibi returns false if nothing found ...
		if ($rowResult == false) {
			return null;
		}

		if ($targetEntityClass != null && !is_subclass_of($targetEntityClass, Entity::class)) {
			throw new InvalidClassException("Unable to convert row to object, class $targetEntityClass does not inherit Entity base class.");
		}

		$targetEntity = $targetEntityClass ?: $this->entityClass;
		return $targetEntity::load($rowResult instanceof \Dibi\Row ? $rowResult->toArray() : $rowResult);
	}

	/**
	 * Helper that converts database query result to target entity class object.
	 * By default it converts the result to objects of `$entityClass` class, see method `rowToObject` for details.
	 * Can be also used for example when handling related entities in your service.
	 */
	protected function rowsToObjects($queryResult, string $targetEntityClass = null) {
		$resultArray = [];
		foreach($queryResult as $row){
			$resultArray[] = $this->rowToObject($row, $targetEntityClass);
		}
		return $resultArray;
	}

	/**
	 * Helper that returns array of ids of given rows. Looks for `id` key.
	 * Rows can contain either database objects or associative array.
	 * Can be used in your service implementation when needed.
	 */
	protected function getIdsOf($rows) {
		$ids = [];
		foreach ($rows as $row) {
			$ids[] = $row instanceof Entity ? $row->id : $row["id"];
		}
		return $ids;
	}

	/**
	 * Helper that saves given object to target table, converting the object's properties to associative array using its `save` method.
	 * By default it saves the object to `mainTable` table, but that can be changed by `targetTable` parameter.
	 * Can be used in your service implementation when needed, for example for storing related entities.
	 */
	protected function saveObjectTo(Entity $objectToSave, string $targetTable = null) {
		$table = $targetTable ?: $this->mainTable;

		if ($objectToSave->id == null) {
			$this->db->query("INSERT INTO %n %v", $table, $objectToSave->save());
			$objectToSave->id = $this->db->insertId;
		} else {
			$this->db->query("UPDATE %n SET %a WHERE id = %i", $table, $objectToSave->save(), $objectToSave->id);
		}

		return $objectToSave;
	}

	/**
	 * Helper to directly convert response of dibi fetch method to object of entityClass associated with this service.
	 * Parameters are same as for dibi::fetch method, see dibi docs.
	 */
	protected function fetchObject($args) {
		return $this->rowToObject($this->db->fetch(func_get_args()));
	}

	/**
	 * Helper to directly convert response of dibi fetchAll method to objects of entityClass associated with this service.
	 * Parameters are same as for dibi::fetch method, see dibi docs.
	 */
	protected function fetchObjects($args) {
		return $this->rowsToObjects($this->db->fetchAll(func_get_args()));
	}
}
