<?php

namespace Basnik\Db;

/**
 * Base entity to inherit database model entities from.
 */
abstract class Entity {

	/** Provides snake case convert method that can be handy for entity implementations */
	use TSnakeCase;

	/** @var int Primary id column */
	public $id;

	/**
	 * Creates model object from given associative array. Methods extractKeys can be used to simplify the process.
	 * @param array $input Associative array to init the object - with fields given mostly from database.
	 * @return Entity
	 */
	abstract public static function load(array $input);

	/**
	 * Returns associative array with properties of this object - array will be used to store the model into database.
	 * Method exportKeys can be used to simplify the process.
	 * @return array
	 */
	abstract public function save() : array;

	/**
	 * Extracts keys from given array and loads them into corresponding properties this object.
	 * Searches for keys with same names or their alternatives in lowercase snake case.
	 * All keys are required, exception will be raised if any key is not present.
	 */
	protected function extractKeys(array $input, ...$keys) {
		foreach ($keys as $key) {
			// if key does not exist, first try underscore alternative, then fail
			if (!array_key_exists($key, $input)) {
				$underscoredKey = $this->toSnakeCase($key);
				if (!array_key_exists($underscoredKey, $input)) {
					throw new KeyMissingException("Key $key (or $underscoredKey) not found while extracting keys from input array.");
				}

				$this->{$key} = $input[$underscoredKey];
				continue;
			}

			// if key exists, simply use it
			$this->{$key} = $input[$key];
		}
	}

	/**
	 * Extracts keys from given array and loads them into corresponding properties this object.
	 * Searches for keys with same names or their alternatives in lowercase snake case.
	 * If key is not found, it is skipped (its value will be null).
	 */
	protected function extractKeysSkipMissing(array $input, ...$keys) {
		foreach ($keys as $key) {
			// if key does not exist, first try underscore alternative
			if (!array_key_exists($key, $input)) {
				$underscoredKey = $this->toSnakeCase($key);
				if (array_key_exists($underscoredKey, $input)) {
					$this->{$key} = $input[$underscoredKey];
				}

				continue;
			}

			// if key exists, simply use it
			$this->{$key} = $input[$key];
		}
	}

	/**
	 * Constructs associative array containing given properties from this object.
	 * Property names are used as keys and transformed to lowercase snake case.
	 * All keys are required, so exception will be raised if any key is not present.
	 */
	protected function exportProps(...$properties) {
		foreach ($properties as $prop) {
			if (!property_exists($this, $prop)) {
				throw new PropertyNotFoundException("Property $prop not found while exporting keys.");
			}

			$target[$this->toSnakeCase($prop)] = $this->{$prop};
		}

		return $target;
	}
}
