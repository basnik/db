<?php
namespace Basnik\Db;

/**
 * Common and helper methods.
 */
trait TSnakeCase {

	/**
	 * Used to convert camel case texts to lowercase snake case texts.
	 * Handy when converting object property names to database column names.
	 * Credits and thanks to Jan Jakeš (https://stackoverflow.com/questions/1993721/how-to-convert-camelcase-to-camel-case)
	 * @return string
	 */
	private function toSnakeCase($input) {
		return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $input)), '_');
	}
}
